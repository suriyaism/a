from application.utilities.utils import json_response


def test_json_response_200():
    response = json_response(status=200,message="Test",data="Test_data")
    assert response is not None
    assert response['statusCode'] == 200
    assert response['body'] == '{"status": 200, "message": "Test", "responseOutput": "Test_data"}'


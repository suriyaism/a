from nose.tools import *
from application.version_2.factorial.lambda_function import lambda_handler, calculate_factorial, factorial_validators


def test_lambda_handler_valid_status():
    event = {'queryStringParameters': {'n': 10}}
    output = lambda_handler(event, None)
    assert output['statusCode'] == 200


@raises(Exception)
def test_lambda_handler_500():
    response = lambda_handler()
    assert response is not None
    assert response['statusCode'] == 500


@raises(ValueError)
def test_calculate_factorial_400():
    n = -1
    response = factorial_validators(n)
    assert response is not None
    assert response['statusCode'] == 400


def test_calculate_factorial_n_0():
    n = 0
    output = calculate_factorial(n)
    assert output == 1


def test_calculate_factorial_n_1():
    n = 1
    output = calculate_factorial(n)
    assert output == 1


def test_calculate_factorial_n_2():
    n = 2
    output = calculate_factorial(n)
    assert output == 2


def test_calculate_factorial_n_6():
    n = 6
    factorial = 720
    output = calculate_factorial(n)
    assert output == factorial


def test_calculate_factorial_n_10():
    n = 10
    factorial = 3628800
    output = calculate_factorial(n)
    assert output == factorial


def test_factorial_validators_n_10():
    n = 10
    output = factorial_validators(n)
    assert output == True


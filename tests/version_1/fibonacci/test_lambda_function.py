from nose.tools import *
from application.version_1.fibonacci.lambda_function import lambda_handler, calculate_fibonacci, fibonacci_validators


def test_lambda_handler_valid_status():
    event = {'queryStringParameters': {'n': 10}}
    output = lambda_handler(event, None)
    assert output['statusCode'] == 200


@raises(Exception)
def test_lambda_handler_500():
    response = lambda_handler()
    assert response is not None
    assert response['statusCode'] == 500


@raises(ValueError)
def test_calculate_fibonacci_400():
    n = 0
    response = fibonacci_validators(n)
    assert response is not None
    assert response['statusCode'] == 400


def test_calculate_fibonacci_n_1():
    n = 1
    output = calculate_fibonacci(n)
    assert output == 0


def test_calculate_fibonacci_n_2():
    n = 2
    output = calculate_fibonacci(n)
    assert output == 1


def test_calculate_fibonacci_n_10():
    n = 10
    fibonacci = 55
    output = calculate_fibonacci(n)
    assert output == fibonacci


def test_calculate_fibonacci_n_100():
    n = 100
    fibonacci = 354224848179261915075
    output = calculate_fibonacci(n)
    assert output == fibonacci

def test_fibonacci_validators_n_20():
    n = 20
    output = fibonacci_validators(n)
    assert output == True
